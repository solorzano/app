'use strict';

/**
* @ngdoc function
* @name htmlApp.controller:AlbumsCtrl
* @description
* # AlbumsCtrl
* Controller of the htmlApp
*/
var mymodal =angular.module('appApp')
mymodal.controller('AlbumsCtrl', function ($scope, $http, Albums) {

		// object to hold all the data for the new comment form
		$scope.albumData = {};

		// loading variable to show the spinning loading icon
		$scope.loading = true;
		
		// get all the comments first and bind it to the $scope.comments object
		Albums.get()
			.success(function(data) {
				$scope.albums = data;
				//$scope.artists = data.artist;
				$scope.loading = false;
				$scope.showModal = false;
				$scope.toggleModal = function(){
					$scope.showModal = !$scope.showModal;
				};
			});


		$scope.showAlbums = function(id){
			$scope.loading = true;

			// get all the comments first and bind it to the $scope.comments object
			Albums.show(id)
				.success(function(data) {
					$scope.albums = data;
					//$scope.artists = data.artist;
					$scope.loading = false;
					
				});

		}

		// function to handle submitting the form
		//$scope.submitAlbums = function() {

		$scope.submitAlbums = function() {
			$scope.loading = true;

			// save the comment. pass in comment data from the form
			Albums.save($scope.albumData)
				.success(function(data) {

					$scope.albumData = {};
					// if successful, we'll need to refresh the comment list
					Albums.get()
						.success(function(getData) {

							//console.log(getData);
							$scope.albums = getData;
							$scope.loading = false;
						});

					/*Albums.get()
						.success(function(getData) {
							$scope.albums = getData;
							$scope.loading = true;
						});
					//console.log(data);
					$scope.albumData = {};
					// if successful, we'll need to refresh the comment list
					Albums.get()
						.success(function(getData) {
							$scope.albums = getData;
							$scope.loading = true;
						});*/

				})
				.error(function(data) {
					console.log(data);
				});
		};



			/*$scope.loading = true;

			// save the comment. pass in comment data from the form
			Albums.save($scope.albumData)
				.success(function(data) {
					console.log(data);
					$scope.albumData = {};
					// if successful, we'll need to refresh the comment list
					Albums.get()
						.success(function(getData) {
							$scope.albums = getData;
							$scope.loading = true;
						});

				})
				.error(function(data) {
					console.log(data);
				});*/
		//};

		// function to handle deleting a comment
		$scope.deleteAlbum = function(id) {
			$scope.loading = true; 

			Albums.destroy(id)
				.success(function(data) {

					// if successful, we'll need to refresh the comment list
					Albums.get()
						.success(function(getData) {
							$scope.albums = getData;
							$scope.loading = true;
						});

				})
				.error(function(data) {
					alert('You can not delete this record, you must first remove the artist..!');
				});
		};
});


mymodal.directive('modal', function () {
	return {
		template: '<div class="modal fade">' + 
		'<div class="modal-dialog">' + 
		'<div class="modal-content">' + 
		'<div class="modal-header">' + 
		'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
		'<h4 class="modal-title">{{ title }}</h4>' + 
		'</div>' + 
		'<div class="modal-body" ng-transclude></div>' + 
		'</div>' + 
		'</div>' + 
		'</div>',
		restrict: 'E',
		transclude: true,
		replace:true,
		scope:true,
		link: function postLink(scope, element, attrs) {
			scope.title = attrs.title;

			scope.$watch(attrs.visible, function(value){
				if(value == true)
					$(element).modal('show');
				else
					$(element).modal('hide');
			});

			$(element).on('shown.bs.modal', function(){
				scope.$apply(function(){
					scope.$parent[attrs.visible] = true;
				});
			});

			$(element).on('hidden.bs.modal', function(){
				scope.$apply(function(){
					scope.$parent[attrs.visible] = false;
				});
			});
		}
	};
});


