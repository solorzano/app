'use strict';

/**
 * @ngdoc function
 * @name htmlApp.controller:ArtistCtrl
 * @description
 * # ArtistCtrl
 * Controller of the htmlApp
 */
var mymodal = angular.module('appApp')
 	mymodal.controller('ArtistCtrl', function ($scope, $http, Artist) {
		// object to hold all the data for the new album form
		$scope.artistData = {};

		// loading variable to show the spinning loading icon
		$scope.loading = true;
		
		// get all the artist first and bind it to the $scope.artist object
		Artist.get()
			.success(function(data) {
				$scope.artist = data;
				//$scope.artists = data.artist;
				$scope.loading = false;
				$scope.showModal = false;
				$scope.toggleModal = function(){
					$scope.showModal = !$scope.showModal;
				};
			});


		$scope.showArtist = function(id){
			$scope.loading = true;

			// get all the artist first and bind it to the $scope.artist object
			Artist.show(id)
				.success(function(data) {
					$scope.artist = data;
					//$scope.artists = data.artist;
					$scope.loading = false;
					
				});

		}

		// function to handle submitting the form
		//$scope.submitartist = function() {

		$scope.submitArtist = function() {
			$scope.loading = true;

			// save the album. pass in album data from the form
			Artist.save($scope.artistData)
				.success(function(data) {

					$scope.artistData = {};
					// if successful, we'll need to refresh the album list
					Artist.get()
						.success(function(getData) {

							//console.log(getData);
							$scope.artist = getData;
							$scope.loading = false;
						});

					/*artist.get()
						.success(function(getData) {
							$scope.artist = getData;
							$scope.loading = true;
						});
					//console.log(data);
					$scope.artistData = {};
					// if successful, we'll need to refresh the album list
					artist.get()
						.success(function(getData) {
							$scope.artist = getData;
							$scope.loading = true;
						});*/

				})
				.error(function(data) {
					console.log(data);
				});
		};

		// function to handle deleting a album
		$scope.deleteArtist = function(id) {
			$scope.loading = true; 

			Artist.destroy(id)
				.success(function(data) {

					// if successful, we'll need to refresh the album list
					Artist.get()
						.success(function(getData) {
							$scope.artist = getData;
							$scope.loading = true;
						});

				})
				.error(function(data) {
					alert('You can not delete this record, you must first remove the artist..!');
				});
		};
  });


mymodal.directive('modal2', function () {
	return {
		template: '<div class="modal fade">' + 
		'<div class="modal-dialog">' + 
		'<div class="modal-content">' + 
		'<div class="modal-header">' + 
		'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
		'<h4 class="modal-title">{{ title }}</h4>' + 
		'</div>' + 
		'<div class="modal-body" ng-transclude></div>' + 
		'</div>' + 
		'</div>' + 
		'</div>',
		restrict: 'E',
		transclude: true,
		replace:true,
		scope:true,
		link: function postLink(scope, element, attrs) {
			scope.title = attrs.title;

			scope.$watch(attrs.visible, function(value){
				if(value == true)
					$(element).modal('show');
				else
					$(element).modal('hide');
			});

			$(element).on('shown.bs.modal', function(){
				scope.$apply(function(){
					scope.$parent[attrs.visible] = true;
				});
			});

			$(element).on('hidden.bs.modal', function(){
				scope.$apply(function(){
					scope.$parent[attrs.visible] = false;
				});
			});
		}
	};
});
