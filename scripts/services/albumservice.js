'use strict';

/**
 * @ngdoc service
 * @name htmlApp.albumService
 * @description
 * # albumService
 * Service in the htmlApp.
 */
angular.module('appApp')
  .service('Albums', function ($http) {
    // AngularJS will instantiate a singleton by calling "new" on this function
	var url = 'http://localhost:8000/api/albums/';
    return {
    	
			get : function() {
				return $http.get(url);
			},
			show : function(id) {
				return $http.get(url + id);
			},
			save : function(albumData) {
				return $http({
					method: 'POST',
					url: url,
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(albumData)
				});
			},
			destroy : function(id) {
				return $http.delete(url + id);
			}
		}
  });
